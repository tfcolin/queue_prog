package queue_prog

import "testing"
import "fmt"

func TestQueueProg(t *testing.T) {

	ns := 5
	total_nu := 25

	var lambda []float64 = []float64 {0.42, 1.2, 1.4, 3.5, 1.8}
	var mu     []float64 = []float64 {0.1, 0.7, 0.8, 1.2, 0.4}
	nu := []int {5, 5, 5, 5, 5}
	var opt_nu []int
	var opt_min_eql float64
	var eql, opt_eql []float64 = make ([]float64, ns), make ([]float64, ns)

	sad := InitServiceAllocateData (ns, total_nu, lambda, mu)
	opt_nu, opt_min_eql = sad.ServiceAllocate ()

	for i := 0; i < ns; i ++ {
		eql[i] = sad.CalEQL (i, nu[i])
		opt_eql[i] = sad.CalEQL (i, opt_nu[i])
	}

	fmt.Printf ("%10s%10s%10s%10s%10s\n", "LOC", "ORI_NU", "ORI_QL", "OPT_NU", "OPT_QL")
	for i := 0; i < ns; i ++ {
		fmt.Printf ("%10d%10d%10.3f%10d%10.3f\n", i, nu[i], eql[i], opt_nu[i], opt_eql[i])
	}
	fmt.Printf ("Opt Min Max Exp Queue Len = %10.3f\n", opt_min_eql)

}
